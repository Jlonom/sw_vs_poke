<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 15.05.18
 * Time: 4:46
 */

namespace app\models\forms;

use yii\base\Model;
use app\models\Votes;

class VoteForm extends Model
{
    public $universe_1;
    public $universe_2;
    public $item_1_id;
    public $item_2_id;
    public $selected;

    public function rules()
    {
        $rules = parent::rules();
        $rules['stringItems'] = [['universe_1', 'universe_2'], 'string'];
        $rules['integerItems'] = [['item_1_id', 'item_2_id'], 'string'];
        $rules['rangeItem'] = ['selected', 'in', 'range' => [1, 2]];
        return $rules;
    }

    public function updateVoices() {
        $item = Votes::find()->where('universe_1 = :u_one AND item_1_id = :item_first AND universe_2 = :u_sec AND item_2_id = :item_sec',
            [
                ':u_one'    =>  $this->universe_1,
                ':item_first'   =>  $this->item_1_id,
                ':u_sec'    =>  $this->universe_2,
                ':item_sec'   =>  $this->item_2_id,
            ])->orWhere('universe_1 = :u_sec AND item_1_id = :item_sec AND universe_2 = :u_one AND item_2_id = :item_first',
            [
                ':u_one'    =>  $this->universe_1,
                ':item_first'   =>  $this->item_1_id,
                ':u_sec'    =>  $this->universe_2,
                ':item_sec'   =>  $this->item_2_id,
            ])->one();

        $voteUp = 'votes_' . $this->selected;
        $item->$voteUp++;
        $item->total_votes++;
        $item->save();

        return sprintf('%d of %d users votes like you!', $item->$voteUp, $item->total_votes);
    }

}