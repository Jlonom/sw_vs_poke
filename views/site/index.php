<?php
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/** @var $randomItems array */
/** @var $oldVote string */
/** @var $model \app\models\forms\VoteForm */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">
        <?php if(!empty($oldVote)){ ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-success"><?= $oldVote ?></div>
                </div>
            </div>
        <?php } ?>
        <?php $form = ActiveForm::begin() ?>
        <div class="row">
                <?= $form->field($model, 'universe_1')->hiddenInput(['value' => $randomItems['api_first']])->label(false)?>
                <?= $form->field($model, 'item_1_id')->hiddenInput(['value' => $randomItems['item_first']['id']])->label(false)?>
                <?= $form->field($model, 'universe_2')->hiddenInput(['value' => $randomItems['api_second']])->label(false)?>
                <?= $form->field($model, 'item_2_id')->hiddenInput(['value' => $randomItems['item_second']['id']])->label(false)?>
                <?= $form->field($model, 'selected')->radioList(
                        [
                            1 => $randomItems['item_first']['item']->name,
                            2 => $randomItems['item_second']['item']->name ],
                        [
                            'item'  =>  function($index, $label, $name, $checked, $value){
                                $return = '<div class="col-md-6"><label class="radio">';
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                $return .= '<h3>' . ucwords($label) . '</h3>';
                                $return .= '</label></div>';

                                return $return;
                            }
                        ]
                )->label(false) ?>
        </div>
        <div class="row">
            <div class="col-xs-12"><?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?></div>
        </div>
        <?php ActiveForm::end() ?>

    </div>
</div>
