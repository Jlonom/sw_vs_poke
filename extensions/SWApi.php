<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 15.05.18
 * Time: 3:38
 */

namespace app\extensions;


class SWApi extends Api
{
    public function setApiUrl() {
        $this->apiUrl = 'https://swapi.co/api/people/';
    }
}