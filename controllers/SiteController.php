<?php

namespace app\controllers;

use app\extensions\PokeApi;
use app\extensions\SWApi;
use app\models\forms\VoteForm;
use app\models\Votes;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     * @var $apiFirst PokeApi|SWApi
     * @return string
     */
    public function actionIndex()
    {
        $randomItems = Votes::getRandItems();
        $model = new VoteForm();
        $oldVote = '';
        if(Yii::$app->request->isPost){
            $model->load(Yii::$app->request->post());
            $oldVote = $model->updateVoices();
        }
        return $this->render('index', ['randomItems' => $randomItems, 'model' => $model, 'oldVote' => $oldVote]);
    }
}
