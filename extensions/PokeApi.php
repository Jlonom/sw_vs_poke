<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 15.05.18
 * Time: 3:56
 */

namespace app\extensions;


class PokeApi extends Api
{
    public function setApiUrl()
    {
        $this->apiUrl = 'http://pokeapi.co/api/v2/pokemon/';
    }
}