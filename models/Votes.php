<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%votes}}".
 *
 * @property int $id
 * @property string $universe_1
 * @property int $item_1_id
 * @property int $votes_1
 * @property string $universe_2
 * @property int $item_2_id
 * @property int $votes_2
 * @property int $total_votes
 */
class Votes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%votes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_1_id', 'votes_1', 'item_2_id', 'votes_2', 'total_votes'], 'integer'],
            [['universe_1', 'universe_2'], 'string', 'max' => 128],
            [['universe_1', 'item_1_id', 'universe_2', 'item_2_id'], 'unique', 'targetAttribute' => ['universe_1', 'item_1_id', 'universe_2', 'item_2_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'universe_1' => 'Universe 1',
            'item_1_id' => 'Item 1 ID',
            'votes_1' => 'Votes 1',
            'universe_2' => 'Universe 2',
            'item_2_id' => 'Item 2 ID',
            'votes_2' => 'Votes 2',
            'total_votes' => 'Total Votes',
        ];
    }

    public static function getRandItems() {
        $apiArray = ['\app\extensions\PokeApi', '\app\extensions\SWApi'];
        $apiClassFirst = $apiArray[mt_rand(0, (count($apiArray) - 1))];
        $apiClassSecond = $apiArray[mt_rand(0, (count($apiArray) - 1))];
        $apiFirst = $apiClassFirst::init();
        $apiSecond = $apiClassSecond::init();

        $firstItem = $apiFirst->getRandomItem();
        $secondItem = $apiFirst->getRandomItem();

        $preVotes = self::find()->where('universe_1 = :u_one AND item_1_id = :item_first AND universe_2 = :u_sec AND item_2_id = :item_sec',
            [
                ':u_one'    =>  $apiClassFirst,
                ':item_first'   =>  $firstItem['id'],
                ':u_sec'    =>  $apiClassSecond,
                ':item_sec'   =>  $secondItem['id'],
            ])->orWhere('universe_1 = :u_one AND item_1_id = :item_first AND universe_2 = :u_sec AND item_2_id = :item_sec',
            [
                ':u_one'    =>  $apiClassSecond,
                ':item_first'   =>  $secondItem['id'],
                ':u_sec'    =>  $apiClassFirst,
                ':item_sec'   =>  $firstItem['id'],
            ])->one();

        if($preVotes === null){
            $preVotes = new self();
            $preVotes->universe_1 = $apiClassFirst;
            $preVotes->item_1_id = $firstItem['id'];
            $preVotes->votes_1 = 0;
            $preVotes->universe_2 = $apiClassSecond;
            $preVotes->item_2_id = $secondItem['id'];
            $preVotes->votes_2 = 0;
            $preVotes->total_votes = 0;
            $preVotes->save();
        }

        return ['api_first' => $apiClassFirst, 'api_second' => $apiClassSecond, 'item_first' => $firstItem, 'item_second' => $secondItem];
    }
}