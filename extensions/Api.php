<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 15.05.18
 * Time: 3:34
 */

namespace app\extensions;


abstract class Api
{
    static $_instance = null;
    public $count;
    public $apiUrl;

    public static function init(){
        if(static::$_instance == null){
            $class = new static();
            $class->setApiUrl();
            $class->setCount();
            static::$_instance = $class;
        }
        return static::$_instance;
    }

    abstract function setApiUrl();

    protected function setCount(){
        $data = static::sendRequest($this->apiUrl);
        $this->count = $data->count;
    }

    public function getRandomItem() {
        while(true){
            $id = rand(1, $this->count);
            $item = static::sendRequest($this->apiUrl . $id . '/');
            if(isset($item->name))
                break;
        }
        return [
            'id'    =>  $id,
            'item'  =>  $item,
        ];
    }

    protected function sendRequest($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYSTATUS, false);
        $response = curl_exec($ch);
        return json_decode($response);
    }
}