<?php

use yii\db\Migration;

/**
 * Handles the creation of table `votes`.
 */
class m180515_002835_create_votes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = '';
        if(Yii::$app->db->driverName == 'mysql')
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{votes}}', [
            'id' => $this->primaryKey(),
            'universe_1'    =>  $this->string(128),
            'item_1_id'     =>  $this->integer(),
            'votes_1'       =>  $this->integer(),
            'universe_2'    =>  $this->string(128),
            'item_2_id'     =>  $this->integer(),
            'votes_2'       =>  $this->integer(),
            'total_votes'   =>  $this->integer(),
        ], $tableOptions);

        $this->createIndex('universe_item_1', '{{votes}}', ['universe_1', 'item_1_id']);
        $this->createIndex('universe_item_2', '{{votes}}', ['universe_2', 'item_2_id']);
        $this->createIndex('unique_universes_items', '{{votes}}', ['universe_1', 'item_1_id', 'universe_2', 'item_2_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('unique_universes_items', '{{votes}}');
        $this->dropIndex('universe_item_2', '{{votes}}');
        $this->dropIndex('universe_item_1', '{{votes}}');
        $this->dropTable('{{votes}}');
    }
}
